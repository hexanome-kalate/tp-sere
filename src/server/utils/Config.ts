import * as fs from "fs";
import Logger from "./Logger";

export default class Config {

    public static get(key: string): any {
        if (false === Config.loaded) {
            Logger.warn("No config file loaded. Providing default config variables.");
        }

        return Config.config[key];
    }

    public static load(path: string): void {
        try {
            Config.config = JSON.parse(fs.readFileSync(path).toString("UTF-8"));
            Config.loaded = true;
            Logger.log("Loaded config file : " + path);
        } catch (err) {
            Logger.error("Unable to load configuration file: " + err);
        }
    }

    private static loaded: boolean = false;

    // TODO : Specify type
    private static config: any = {
        port: 80,
        webroot: "webroot",
        sessionSecret: "blobfish",
    };

}