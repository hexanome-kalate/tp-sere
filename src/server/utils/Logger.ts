import chalk from "chalk";

const baseLogger = console;

export default class Logger {

    public static log(message: string) {
        baseLogger.log("[INFO]" + Logger.header + message);
    }

    public static warn(message: string) {
        baseLogger.log(chalk.yellow("[WARN]" + Logger.header + message));
    }

    public static error(message: string) {
        baseLogger.log(chalk.red("[ERROR]" + Logger.header + message));
    }

    public static debug(...data: any[]) {
        baseLogger.log(chalk.cyan("[DEBUG]" + Logger.header + data.join("\n    ")));
    }

    public static info(message: string) {
        baseLogger.log(chalk.greenBright("[INFO]" + Logger.header + message));
    }

    private static header: string = "[TP-SERE] ";
}