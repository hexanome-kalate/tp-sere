import * as bodyParser from "body-parser";
import * as express from "express";
import * as session from "express-session";
import * as path from "path";
import Logger from "./utils/Logger";
import Config from "./utils/Config";
import UserController from "./controllers/UserController";

const log = Logger.log;
const server = express();

// Load config file
Config.load(path.join(process.cwd(), "/appconfig.json"));

// Register useful middlewares
server.use(session({
    secret: Config.get('sessionSecret'),
    resave: false,
    saveUninitialized: false
}));

server.use(bodyParser.urlencoded({extended: true}));
server.use(bodyParser.json());

// Register public directory, which serves static files
log("Registering static files directory: "+ Config.get('webroot'));

server.use(express.static(Config.get('webroot')));

// Register routes with applicative actions
log("Registering routes...");

UserController.register(server);

// Start listening some HTTP requests.
server.listen(Config.get('port'));

log(`Express server ready for orders on port ${Config.get('port')} ! \\o/`);