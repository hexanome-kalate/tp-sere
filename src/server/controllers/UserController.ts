import {Express, Request, Response} from "express";
import Config from "../utils/Config";
import User from "../../entities/User";
import Logger from "../utils/Logger";

const ERR_INVALID_CREDENTIALS = "The input credentials are invalid. Please check and retry.";

export default class UserController {

    public static register(express: Express) {
        express.get('/user/data', UserController.data);
        express.post('/user/login', UserController.login);
        express.get('/user/logout', UserController.logout);
    }

    public static data(req: Request, res: Response) {
        let user: User;
        if (req.session && req.session.logged) {
            user = new User(true, Config.get("user").username, req.session.ip);
        } else {
            user = new User(false, "", "");
        }

        res.send({user});
    }

    public static login(req: Request, res: Response) {
        Logger.debug(`User with IP ${req.ip} trying to log in.`);

        if (req.session && req.session.logged) {
            // We are already connected. As fallback, we provide user session data.
            UserController.data(req, res);
            return;
        }

        const username = req.body.username;
        const password = req.body.password;
        const realUserdata = Config.get("user");

        Logger.debug(`Credentials sent:`, username, password);

        if (req.session && username === realUserdata.username && password === realUserdata.password) {
            Logger.info(`User with IP ${req.ip} logged in successfully.`);
            req.session.logged = true;
            req.session.ip = req.ip;
            res.send({user: new User(true, realUserdata.username, req.session.ip)});
        } else {
            Logger.info(`User with IP ${req.ip} FAILED to log in.`);
            res.send({errors: [ERR_INVALID_CREDENTIALS]});
        }

    }

    public static logout(req: Request, res: Response) {
        Logger.info(`Clearing user with session ID ${req.sessionID}`);

        if (req.session) {
            req.session.destroy(() => {
                res.send({status: true});
            });
        }
    }

}