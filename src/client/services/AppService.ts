import axios from "axios";
import AbstractService from "./AbstractService";
import User from "../../entities/User";
import Config from "../config";


const ERR_INVALID_SERVER_RESPONSE = "The server returned an invalid response. Please try again.";

export default class AppService extends AbstractService {

    constructor(private entrypoint:string) {
        super();
    }

    public async fetchUserSession(): Promise<{user?: User, errors: string[]}> {
        let errors: string[] = [];
        let response;

        try {
            response = await axios.get(this.entrypoint + Config.ENDPOINT_USERDATA);

            if (response.status != 200) {
                errors.push(ERR_INVALID_SERVER_RESPONSE);
                return {errors};
            }

            return {user: <User>response.data.user, errors};

        } catch (err) {
            errors.push(ERR_INVALID_SERVER_RESPONSE);
            return {errors};
        }
    }

    public async connect(username: string, password: string): Promise<{user?: User, errors: string[]}> {
        let errors: string[] = [];

        try {
            let response = await axios.post(this.entrypoint + Config.ENDPOINT_USERLOGIN, {username, password});

            console.debug(response);

            if (!response.data.user) {
                response.data.user = {loggedIn: false, username: "", loginIp: ""};
            }
            if (response.data.errors) {
                errors.push(...response.data.errors);
            }

            return {user: new User(response.data.user.loggedIn, response.data.user.username, response.data.user.loginIp), errors};
        } catch (err) {
            errors.push(ERR_INVALID_SERVER_RESPONSE);
            return {errors};
        }
    }

    public async disconnect() {
        await axios.get(this.entrypoint + Config.ENDPOINT_USERLOGOUT);
    }


}