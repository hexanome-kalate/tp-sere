import AbstractService from "./AbstractService";

export default class ServiceRegistry {

    private registry: {[key: string]: AbstractService};

    private static instance: ServiceRegistry;

    constructor() {
        this.registry = {};
    }

    private static i() {
        if (!ServiceRegistry.instance) {
            ServiceRegistry.instance = new ServiceRegistry();
        }
        return ServiceRegistry.instance;
    }

    public static register(key: string, registeringClass: () => AbstractService|typeof AbstractService): void {
        const instance = ServiceRegistry.i();
        if (typeof registeringClass === "function") {
            instance.registry[key] = registeringClass();
        } else {
            instance.registry[key] = new (<typeof AbstractService>registeringClass)();
        }
    }

    public static get(key: string): undefined|AbstractService {
        return ServiceRegistry.i().registry[key];
    }

}