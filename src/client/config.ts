export default class Config {
    public static readonly API_ENTRYPOINT: string = "";
    public static readonly ENDPOINT_USERDATA: string = '/user/data';
    public static readonly ENDPOINT_USERLOGIN: string = '/user/login';
    public static readonly ENDPOINT_USERLOGOUT: string = '/user/logout';
    public static readonly KITTY_URL: string = Config.API_ENTRYPOINT + '/img/kitty.jpg';
    public static readonly XSS_URL: string = Config.API_ENTRYPOINT + "/xss/demo";
}