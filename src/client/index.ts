import AppService from "./services/AppService";
import Vue from "vue";
import VueRouter from "vue-router";
import routes from "./routes";
import ServiceRegistry from "./services/ServiceRegistry";
import Config from './config';
import Menu from './components/Menu.vue'

ServiceRegistry.register('app', () => {
    return new AppService(Config.API_ENTRYPOINT);
});

Vue.use(VueRouter);
Vue.component('app-menu', Menu);

const router = new VueRouter({routes});

new Vue({
    router
}).$mount('#app');