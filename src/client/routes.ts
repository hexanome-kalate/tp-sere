import Home from "./components/home/Home.vue"
import Login from "./components/login/Login.vue"
import CSRF from "./components/csrf/CSRF.vue"
import XSS from "./components/xss/XSS.vue";

export default [
    { path: '/', component: Home},
    { path: '/login', component: Login},
    { path: '/csrf', component: CSRF},
    { path: '/xss', component: XSS}
];