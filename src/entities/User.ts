export default class User {
    constructor(public loggedIn: boolean, public username: string, public loginIp: string) {}
}