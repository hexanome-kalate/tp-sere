# TP-SERE

TP-SERE is a basic platform, explaining to junior web developers classic exploits they must be aware of.

This project has been designed by Kalate Hexanome, INSA Lyon, 2018.

## Requirements and Installation

`Node.js` and `npm` are required to use this platform.

Once installed, please run the following command :
```bash
npm install
```

Please copy `appconfig.example.json` to `appconfig.json`, define the port the server will listen to.

Then, to build the client and run the server, simply type:
```bash
npm start
```

**Note:** If you are developing something in the client, you may want to use the (very) useful autoreload feature.
To do so, you will need to run server and client compilation separately.
```bash
# Terminal 1
npm run start-server

# Terminal 2
npm run watch-client-js
```
